% Licensed under the Apache License, Version 2.0 (the "License"); you may not
% use this file except in compliance with the License. You may obtain a copy of
% the License at
%
%   http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations under
% the License.

-module(coffer_backend_proplist).

-behaviour(coffer_backend).

-export([init/1]).
-export([terminate/1]).
-export([put/3]).
-export([get/2]).
-export([foldl/3]).
-export([delete/2]).
-export([exists/2]).
-export([validate_id/1]).

init(_Options) ->
    {ok, []}.

terminate(_State) ->
    ok.

put(Id, Data, StorageState) ->
    case proplists:get_value(Id, StorageState) of
        undefined ->
            NewStorageState = [{Id, Data}|StorageState],
            {ok, NewStorageState};
        _ ->
            {ok, StorageState}
    end.

get(Id, StorageState) ->
    case proplists:get_value(Id, StorageState) of
        undefined ->
            {error, not_found};
        Data ->
            {ok, Data}
    end.

foldl(FoldFun, Acc, StorageState) ->
    Result = lists:foldl(
        fun({Id, _Data}, LAcc) ->
            [FoldFun(Id)|LAcc]
        end,
        Acc,
        StorageState),
    {ok, Result}.

delete(Id, StorageState) ->
    NewStorageState = proplists:delete(Id, StorageState),
    {ok, NewStorageState}.

exists(Id, StorageState) ->
    proplists:is_defined(Id, StorageState).

validate_id(_) ->
    true.

