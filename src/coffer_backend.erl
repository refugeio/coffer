% Licensed under the Apache License, Version 2.0 (the "License"); you may not
% use this file except in compliance with the License. You may obtain a copy of
% the License at
%
%   http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations under
% the License.

-module(coffer_backend).

-include("coffer.hrl").

-callback init(Options::list()) ->
    {ok, StorageState::storage_state()} |
    {error, Reason::atom()}.

-callback terminate(StorageState::storage_state()) ->
    ok.

-callback put(Id::blob_id(), Data::blob_data(), StorageState::storage_state()) ->
    {ok, NewStorageState::storage_state()} |
    {error, Reason::atom()}.

-callback get(Id::blob_id(), StorageState::storage_state()) ->
    {ok, Data::blob_data()} |
    {error, Reason::atom()}.

-callback foldl(FoldFun::fun(), Acc::list(), StorageState::storage_state()) ->
    {ok, Result::list()} |
    {error, Reason::atom()}.

-callback delete(Id::blob_id(), StorageState::storage_state()) ->
    {ok, NewStorageState::storage_state()} |
    {error, Reason::atom()}.

-callback exists(Id::blob_id(), StorageState::storage_state()) -> boolean().

-callback validate_id(Id::blob_id()) -> boolean().

