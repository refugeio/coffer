% Licensed under the Apache License, Version 2.0 (the "License"); you may not
% use this file except in compliance with the License. You may obtain a copy of
% the License at
%
%   http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations under
% the License.

-module(coffer_example).

-export([main/0]).

main() ->
    io:format("Starting Coffer sample!~n"),

    io:format("starting a storage!~n"),
    {ok, Pid} = coffer:open(proplist, []),

    Bin1 = <<"Hello">>,
    Id1 = coffer_util:compute_id(Bin1),

    Bin2 = <<"World">>,
    Id2 = coffer_util:compute_id(Bin2),

    coffer:put(Pid, Id1, Bin1),
    coffer:put(Pid, Id1, Bin1),

    coffer:put(Pid, Id2, Bin2),

    display_list(Pid),

    io:format("closing a storage~n"),
    coffer:close(Pid),

    ok.

%% ---

display_list(Pid) ->
    {ok, Blobs} = coffer:foldl(
        fun(BlobId) ->
                BlobId
        end,
        [],
        Pid),
    io:format("Current blobs for pid: ~p: ~p~n", [Pid, Blobs]),
    ok.

