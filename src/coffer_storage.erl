% Licensed under the Apache License, Version 2.0 (the "License"); you may not
% use this file except in compliance with the License. You may obtain a copy of
% the License at
%
%   http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations under
% the License.

-module(coffer_storage).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

-record(storage, {
        backend,
        options = [],
        state = {}
    }).

init({BackendType, Options}) ->
    {ok, StorageState} = BackendType:init(Options),
    State = #storage{backend=BackendType, options=Options, state=StorageState},
    {ok, State}.

handle_call({stop}, _From, State) ->
    {stop, normal, ok, State};
handle_call({put, Id, Data}, _From, #storage{backend=BackendType, state=StorageState}=State) ->
    case BackendType:validate_id(Id) of
        true ->
            case BackendType:put(Id, Data, StorageState) of
                {ok, NewStorageState} ->
                    Reply = {ok, Id},
                    NewState = State#storage{state=NewStorageState},
                    {reply, Reply, NewState};
                {error, Reason} ->
                    {reply, {error, Reason}, State}
            end;
        _ ->
            {error, invalid_id}
    end;
handle_call({get, Id}, _From, #storage{backend=BackendType, state=StorageState}=State) ->
    Reply = BackendType:get(Id, StorageState), 
    {reply, Reply, State};
handle_call({foldl, FoldFun, Acc}, _From, #storage{backend=BackendType, state=StorageState}=State) ->
    Reply = BackendType:foldl(FoldFun, Acc, StorageState),
    {reply, Reply, State};
handle_call({delete, Id}, _From, #storage{backend=BackendType, state=StorageState}=State) ->
    case BackendType:delete(Id, StorageState) of
        {ok, NewStorageState} ->
            NewState = State#storage{state=NewStorageState},
            {reply, ok, NewState};
        {error, Reason} ->
            {reply, {error, Reason}, State}
    end;
handle_call({exists, Id}, _From, #storage{backend=BackendType, state=StorageState}=State) ->
    Reply = BackendType:exists(Id, StorageState),
    {reply, Reply, State};
handle_call(Request, From, State) ->
    io:format("Unknown request [~p] from ~p~n", [Request, From]),
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, #storage{backend=BackendType, state=StorageState}) ->
    ok = BackendType:terminate(StorageState),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

