% Licensed under the Apache License, Version 2.0 (the "License"); you may not
% use this file except in compliance with the License. You may obtain a copy of
% the License at
%
%   http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations under
% the License.

-module(coffer_backend_null).

-behaviour(coffer_backend).

-export([init/1]).
-export([terminate/1]).
-export([put/3]).
-export([get/2]).
-export([foldl/3]).
-export([delete/2]).
-export([exists/2]).
-export([validate_id/1]).

init(_Options) ->
    {ok, []}.

terminate(_State) ->
    ok.

put(_Id, _Data, _StorageState) ->
    {ok, []}.

get(_Id, _StorageData) ->
    {ok, <<>>}.

foldl(_FoldFun, _Acc, _StorageState) ->
    {ok, []}.

delete(_Id, StorageState) ->
    {ok, StorageState}.

exists(_Id, _StorageState) ->
    false.

validate_id(_) ->
    true.

