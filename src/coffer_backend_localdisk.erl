% Licensed under the Apache License, Version 2.0 (the "License"); you may not
% use this file except in compliance with the License. You may obtain a copy of
% the License at
%
%   http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations under
% the License.

-module(coffer_backend_localdisk).

-behaviour(coffer_backend).

-export([init/1]).
-export([terminate/1]).
-export([put/3]).
-export([get/2]).
-export([foldl/3]).
-export([delete/2]).
-export([exists/2]).
-export([validate_id/1]).

init([{root, RootDir}]) ->
    %% TODO need to check the RootDir exists!
    {ok, RootDir};
init(_) ->
    {stop, wrong_config}.

terminate(_State) ->
    ok.

put(Id, Data, RootDir) ->
    {Path, Filename} = path_n_filename_for_id(RootDir, Id),
    Fullname = full_filename(Path, Filename),

    case filelib:is_file(Fullname) of
        true ->
            {ok, RootDir};
        false ->
            ok = ensure_path_exists(Path),
            case file:open(Fullname, [binary, write]) of
                {error, Reason} ->
                    {error, Reason};
                {ok, IoDevice} ->
                    try 
                        case file:write(IoDevice, Data) of
                            ok ->
                                {ok, RootDir};
                            {error, Reason} ->
                                {error, Reason}
                        end
                        after file:close(IoDevice)
                    end
            end
    end.

get(Id, RootDir) ->
    {Path, Filename} = path_n_filename_for_id(RootDir, Id),
    Fullname = full_filename(Path, Filename),

    case filelib:is_file(Fullname) of
        false ->
            {error, not_found};
        true ->
            case file:read_file(Fullname) of
                {error, Reason} ->
                    {error, Reason};
                {ok, Data} ->
                    {ok, Data}
            end
    end.

foldl(FoldFun, Acc, RootDir) ->
    Result = filelib:fold_files(
        RootDir,
        ".*",
        true, 
        fun(Filename, LAcc) ->
            Id = compute_id_from_filename(RootDir, Filename),
            [FoldFun(Id)|LAcc]
        end,
        Acc),
    {ok, Result}.

delete(Id, RootDir) ->
    {Path, Filename} = path_n_filename_for_id(RootDir, Id),
    Fullname = full_filename(Path, Filename),

    case filelib:is_file(Fullname) of
        false ->
            ok; %% nothing to do
        true ->
            file:delete(Fullname)
    end,

    {ok, RootDir}.

exists(Id, RootDir) ->
    {Path, Filename} = path_n_filename_for_id(RootDir, Id),
    Fullname = full_filename(Path, Filename),

    filelib:is_file(Fullname).

validate_id(Id) ->
    size(Id) >= 7.

%% ---

path_n_filename_for_id(RootDir, Id) ->
    <<First:2/binary, Second:2/binary, Third:2/binary, Filename/binary>> = Id,
    Path = <<RootDir/binary, $/, First/binary, $/, Second/binary, $/, Third/binary>>,
    {Path, Filename}.

ensure_path_exists(Path) ->
    DummyFile = <<Path/binary, $/, <<"whocares">>/binary>>,
    filelib:ensure_dir(DummyFile).

full_filename(Path, Filename) ->
    <<Path/binary, $/, Filename/binary>>.

compute_id_from_filename(RootDir, Filename) ->
    FB = iolist_to_binary(Filename),
    RelativeFB = binary:replace(FB, <<RootDir/binary, $/>>, <<>>),
    <<First:2/binary, $/, Second:2/binary, $/, Third:2/binary, $/, Rest/binary>> = RelativeFB,
    <<First/binary, Second/binary, Third/binary, Rest/binary>>.

