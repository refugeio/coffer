% Licensed under the Apache License, Version 2.0 (the "License"); you may not
% use this file except in compliance with the License. You may obtain a copy of
% the License at
%
%   http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations under
% the License.

-module(coffer).

-include("coffer.hrl").

-export([list_storages/0]).
-export([open/2, open/1]).

-export([put/3]).
-export([get/2]).
-export([foldl/3]).
-export([delete/2]).
-export([exists/2]).

-export([close/1]).

-define(STORAGES, [
        {null, coffer_backend_null},
        {proplist, coffer_backend_proplist},
        {localdisk, coffer_backend_localdisk}
    ]).

%% @doc Returns the list of available storages.
-spec list_storages() -> list().
list_storages() ->
    lists:foldl(
        fun({K, _V}, Acc) ->
            [K|Acc]
        end,
        [],
        ?STORAGES).

%% @doc Opens a new storage.
-spec open(BackendType::atom(), Options::list()) ->
    {ok, storage()} | {error, term()}.
open(BackendType, Options) ->
    case name_to_module(BackendType) of
        error ->
            {error, wrong_type};
        Module ->
             gen_server:start(coffer_storage, {Module, Options}, [])
    end.
open(BackendType) ->
    open(BackendType, []).

%% ---

%% @doc Store a blob in the given storage.
-spec put(Pid::storage(), Id::blob_id(), Data::blob_data()) ->
    ok | {error, term()}.
put(Pid, Id, Data) ->
    gen_server:call(Pid, {put, Id, Data}).

%% @doc Retrieves a blob from a storage based on its id.
-spec get(Pid::storage(), Id::blob_id()) ->
    {ok, blob_data()} | {error, term()}.
get(Pid, Id) ->
    gen_server:call(Pid, {get, Id}).

%% @doc Closes the storage.
-spec close(Pid::storage()) -> ok.
close(Pid) ->
    gen_server:call(Pid, {stop}).

%% @doc Executes the given fun on all blob ids present in the storage.
-spec foldl(FoldFun::fun(), Acc::list(), Pid::storage()) ->
        {ok, list()} | {error, term()}.
foldl(FoldFun, Acc, Pid) ->
    gen_server:call(Pid, {foldl, FoldFun, Acc}).

%% @doc Deletes an existing blob.
-spec delete(Pid::storage(), Id::blob_id()) -> ok | {error, term()}.
delete(Pid, Id) ->
    gen_server:call(Pid, {delete, Id}).

%% @doc Returns true if a blob with a given id exists, false otherwise.
-spec exists(Pid::storage(), Id::blob_id()) -> boolean().
exists(Pid, Id) ->
    gen_server:call(Pid, {exists, Id}).

%% Internal API

name_to_module(Name) ->
    case proplists:get_value(Name, ?STORAGES) of
        undefined -> error;
        Module -> Module
    end.

